const model = require('./model');

class Services {
    constructor(){
        this.matches = [];
        this.games = [];
        let daddy = new model.Match('id45785',[],[],"type1");
        this.matches.push(daddy);
    };

    getMatchesByType(type){
        let matchesToReturn = [];
        for(let i =0 ; i < this.matches.length; i++) {
            if(this.matches[i].gameplayType == type) {
                matchesToReturn.push(this.matches[i]);
            }
        }
        return matchesToReturn;
    }

    addMatch(gameplayType, balls){
        let id = new Date().getTime().toString();
        let result = new model.Match(id,[],balls,gameplayType);
        this.matches.push(result);
        this.games.push(new Game(result));
        return result;
    }

    addPlayerToMatch(idMatch,socket,player){
        let game = this.getGameByMatch(idMatch);
        game.match.players.push(player);
        game.sockets.push(socket);
        return game;
    }

    getGameByMatch(idMatch){
        for(let i =0; i<this.games.length; i++){
            let currentGame = this.games[i];
            if(currentGame.match.id == idMatch){
                return currentGame;
            }
        }
        return null;
    }

    getGameBySocket(socket){
        for(let i =0; i<this.games.length; i++){
            let currentGame = this.games[i];
            for(let j =0; j<currentGame.sockets.length; j++){
                if(currentGame.sockets[j] == socket){
                    return currentGame;
                }
            }
        }
        return null;
    }
}

class Game {
    constructor(match){
        this.match = match;
        this.sockets = [];
    }
}

module.exports = new Services();