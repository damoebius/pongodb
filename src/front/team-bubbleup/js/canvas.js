var stage = new createjs.Stage("canvas");

var canvas = document.getElementById("canvas");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

// Environement
let joueur1, joueur2, ball;
let bricks = [];
var delta1 = 0;
var delta2 = 0;

//JOUEURS
// Joueur 1
joueur1 = new createjs.Shape();
joueur1.graphics.beginFill("black").drawRect(0, 0, 100, 20);

joueur1.x = (canvas.height/2) - 50;
joueur1.y = 40;

stage.addChild(joueur1);

// Joueur 2
joueur2 = new createjs.Shape();
joueur2.graphics.beginFill("red").drawRect(0, 0, 100, 20);

joueur2.x = (canvas.height/2) - 50;
joueur2.y = canvas.height-60;

stage.addChild(joueur2);

//ball
ball = new createjs.Shape();
ball.graphics.beginFill("blue").drawCircle(-5, -5, 20);
ball.x = window.innerWidth/2;
ball.y = window.innerHeight/2;

stage.addChild(ball);

//CONTROLS
document.onkeydown = function() {
  controlsPad1();
  controlsPad2();
};

function controlsPad1(e) {
  e = window.event;
  if (e.keyCode == '37') {
    console.log("left");
    delta1 = -1;
  }
  else if (e.keyCode == '39') {
    console.log("right");
    delta1 = 1;
  }
}

function controlsPad2(e) {
  e = window.event;
  if (e.keyCode == '81') {
    console.log("left");
    delta2 = -1;
  }
  else if (e.keyCode == '68') {
    console.log("right");
    delta2 = 1;
  }
}


// ANIMATION
function animation() {
  var vitesse = 10;

  if(joueur1.x >= window.innerWidth || joueur1.x < 0) {
    delta1 *= -1;
  }
  joueur1.x += vitesse*delta1;

  if(joueur2.x >= window.innerWidth || joueur2.x < 0) {
    delta2 *= -1;
  }
  joueur2.x += vitesse*delta2;

  requestAnimationFrame(animation);
}
animation();


//REBOND BORDS
var ballSpeed = 8.0;
var ballXVector = 1;
var ballYVector = 1;

console.log(ball);

function moveBall(){

  var nextYPosition = ball.y + (ballSpeed*ballYVector);
  var nextXPosition = ball.x + (ballSpeed*ballXVector);

  if(nextYPosition > stage.canvas.height || nextYPosition <= 0) {
    ballYVector *= -1;
    color = new TimelineMax();
    color.add(
      TweenMax.to(ball.graphics._fill, .1, {style: "green"}),
    ).add(
      TweenMax.to(ball.graphics._fill, .1, {style: "blue"}),
    );
  }
  ball.y =  nextYPosition;

  if(nextXPosition > stage.canvas.width || nextXPosition <= 0) {
    ballXVector *= -1;
    color = new TimelineMax();
    color.add(
      TweenMax.to(ball.graphics._fill, .1, {style: "green"}),
    ).add(
      TweenMax.to(ball.graphics._fill, .1, {style: "blue"}),
    );
  }
  ball.x =  nextXPosition;

  if(joueur1.x < ball.x < joueur1.x + 100 && ball.y <= joueur1.y + 20) {
    ballXVector *= -1;
  };

}

function handleTick(event) {
  // controlsPad();
  moveBall();
  stage.update();
  // console.log(joueur1);
}

createjs.Ticker.framerate  = 60;
createjs.Ticker.addEventListener("tick", handleTick);

stage.addEventListener('stagemousemove',function(event){
    joueur1.x = event.stageX;
    // joueur1.y = event.stageY;
});
