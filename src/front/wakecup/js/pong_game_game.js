var ballXVector = 1;
var ballYVector = 1;

function xHitTest(nextPos){ 
    var result = false; 
    if(ball.x < paddle.x - 30 && nextPos >= paddle.x - 30 && (ball.y > paddle.y -115 && ball.y < paddle.y +115) ){ 
        result = true; 
    } 
    if(ball.x  > paddle.x + 30 && nextPos <= paddle.x + 30 && (ball.y > paddle.y -115 && ball.y < paddle.y +115) ){ 
        result = true; 
    } 
    return result; 
}

function xHitTestInvit(nextPos){ 
    var result = false; 
    if(ball.x < paddleInvit.x - 30 && nextPos >= paddleInvit.x - 30 && (ball.y > paddleInvit.y -115 && ball.y < paddleInvit.y +115) ){ 
        result = true; 
    } 
    if(ball.x  > paddleInvit.x + 30 && nextPos <= paddleInvit.x + 30 && (ball.y > paddleInvit.y -115 && ball.y < paddleInvit.y +115) ){ 
        result = true; 
    } 
    return result; 
}


function moveBall(){

    var nextYPosition = ball.y + (ballSpeed*ballYVector);
    var nextXPosition = ball.x + (ballSpeed*ballXVector);

    if(nextYPosition > stage.canvas.height
    || nextYPosition <= 0
    ){
        ballYVector *= -1;
        playSound();
    }

    ball.y =  nextYPosition;

    if(nextXPosition > stage.canvas.width -  30
    || nextXPosition <= 30 
    ){
        ballXVector *= -1;
        playSound();
    }

    if(xHitTest(nextXPosition) || xHitTestInvit(nextXPosition)){
            ballXVector *= -1.05;
            playSound();
    }

    ball.x =  nextXPosition;    
}

var vitessepaddle = 15;
var player1Press = {bas:0, haut:0};
var upPress = 0;

window.onkeydown=keyPress;
window.onkeyup=keyRelease;

function keyPress(e){
    if (e.keyCode == 40){
        player1Press.bas = 1;
    }
    if (e.keyCode == 38){
        player1Press.haut = 1;
    }	
}

function keyRelease(e){
    if (e.keyCode == 40){
        player1Press.bas = 0;
    }
    if (e.keyCode == 38){
        player1Press.haut = 0;
    }
}

function deplacement(){
    if (player1Press.bas == 1){
        paddleInvit.y = paddleInvit.y+vitessepaddle;
    }
    if (player1Press.haut == 1){
        paddleInvit.y = paddleInvit.y-vitessepaddle;
    }
}

function handleTick(event) {
    deplacement();
    moveBall();
    stage.update();
}

createjs.Ticker.framerate  = 60;
createjs.Ticker.addEventListener("tick", handleTick);

stage.addEventListener('stagemousemove',function(event){
    // paddle.x = event.stageX;
    paddle.y = event.stageY;
})


