var started = false;
var ballSpeed = 8.0;
var ballXVector = 1;
var ballYVector = 1;
var ball = new createjs.Container();
var stage = new createjs.Stage("demoCanvas");
var paddle = new createjs.Container();
var paddleInvit = new createjs.Container();
var background = new createjs.Bitmap("images/background.png");

var initGameView = function () {
    background.scale = 0.435;
    
    var paddle = new createjs.Container();
    var paddleBitmap = new createjs.Bitmap("images/raquette.png");
    paddle.addChild(paddleBitmap);
    paddleBitmap.x = -31;
    paddleBitmap.y = -115;
    paddleBitmap.scale = 0.6;
    
    paddle.x = 320;
    paddle.y = 350;
    
    var paddleInvit = new createjs.Container();
    var paddleBitmapInvit = new createjs.Bitmap("images/raquetteInvit.png");
    paddleInvit.addChild(paddleBitmapInvit);
    paddleBitmapInvit.x = -31;
    paddleBitmapInvit.y = -115;
    paddleBitmapInvit.scale = 0.6;
    
    paddleInvit.x = 1100;
    paddleInvit.y = 350;
    
    var ball = new createjs.Container();
    var ballBitmap = new createjs.Bitmap("images/ball.png");
    ball.addChild(ballBitmap);
    ballBitmap.x = -24;
    ballBitmap.y = -31;
    ball.scale = 0.8;
    
    ball.x = 700 - 24;
    ball.y = 400 - 31;
    
    stage.addChild(background);
    stage.addChild(paddleInvit);
    stage.addChild(paddle);
    stage.addChild(ball);

    createjs.Ticker.framerate  = 60;
    createjs.Ticker.addEventListener("tick", handleTick);

    }



    var handleTick = function (event) {
        if(started){
            moveBall();
        }
        stage.update();
    }
    
    var startGame = function(){
        init();
        started = true;
    }
    
    var init = function(){
        socket.on(MoveBallMessage.type, function (message) {
            ball.x = message.x;
            ball.y = message.y
        });
    }
    
    function moveBall() {
    
        var nextYPosition = ball.y + (ballSpeed * ballYVector);
        var nextXPosition = ball.x + (ballSpeed * ballXVector);
    
        if (nextYPosition > stage.canvas.height
            || nextYPosition <= 0
        ) {
            ballYVector *= -1;
        }
    
        if (nextXPosition > stage.canvas.width
            || nextXPosition <= 0
        ) {
            ballXVector *= -1;
        }
    
        socket.emit(MoveBallRequest.type, new MoveBallRequest(match.balls[0],nextXPosition,nextYPosition,0))
    
    }

//     var handleTick = function (event) {
//         if(started){
//             moveBall();
//             deplacement();
//         }
//         stage.update();
//     }
    
//     var startGame = function(){
//         init();
//         started = true;
//     }
    
//     var init = function(){
//         socket.on(MoveBallMessage.type, function (message) {
//             ball.x = message.x;
//             ball.y = message.y
//         });
//     }
    
    

// function xHitTest(nextPos){ 
//     var result = false; 
//     if(ball.x < paddle.x - 30 && nextPos >= paddle.x - 30 && (ball.y > paddle.y -115 && ball.y < paddle.y +115) ){ 
//         result = true; 
//     } 
//     if(ball.x  > paddle.x + 30 && nextPos <= paddle.x + 30 && (ball.y > paddle.y -115 && ball.y < paddle.y +115) ){ 
//         result = true; 
//     } 
//     return result; 
// }

// function xHitTestInvit(nextPos){ 
//     var result = false; 
//     if(ball.x < paddleInvit.x - 30 && nextPos >= paddleInvit.x - 30 && (ball.y > paddleInvit.y -115 && ball.y < paddleInvit.y +115) ){ 
//         result = true; 
//     } 
//     if(ball.x  > paddleInvit.x + 30 && nextPos <= paddleInvit.x + 30 && (ball.y > paddleInvit.y -115 && ball.y < paddleInvit.y +115) ){ 
//         result = true; 
//     } 
//     return result; 
// }


// function moveBall() {

//     var nextYPosition = ball.y + (ballSpeed * ballYVector);
//     var nextXPosition = ball.x + (ballSpeed * ballXVector);

//     if (nextYPosition > stage.canvas.height
//         || nextYPosition <= 0
//     ) {
//         ballYVector *= -1;
//     }

//     if (nextXPosition > stage.canvas.width
//         || nextXPosition <= 0
//     ) {
//         ballXVector *= -1.01;
//     }

//     socket.emit(MoveBallRequest.type, new MoveBallRequest(match.balls[0],nextXPosition,nextYPosition,0))

// }

// var vitessepaddle = 15;
// var player1Press = {bas:0, haut:0};
// var upPress = 0;

// window.onkeydown=keyPress;
// window.onkeyup=keyRelease;

// function keyPress(e){
//     if (e.keyCode == 40){
//         player1Press.bas = 1;
//     }
//     if (e.keyCode == 38){
//         player1Press.haut = 1;
//     }	
// }

// function keyRelease(e){
//     if (e.keyCode == 40){
//         player1Press.bas = 0;
//     }
//     if (e.keyCode == 38){
//         player1Press.haut = 0;
//     }
// }

// function deplacement(){
//     if (player1Press.bas == 1){
//         paddleInvit.y = paddleInvit.y+vitessepaddle;
//     }
//     if (player1Press.haut == 1){
//         paddleInvit.y = paddleInvit.y-vitessepaddle;
//     }
//     stage.addEventListener('stagemousemove',function(event){
//         // paddle.x = event.stageX;
//         paddle.y = event.stageY;
//     })
// }







