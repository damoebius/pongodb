console.log("client script");

var getUID = function(){
    return new Date().getTime();
}

var serverip = "localhost";
var gameplayType = "wakecup";
var match;
var currentPlayer = new Player(getUID(), 0, 0, 0, 0, "host");
var host = false;

const socket = io("ws://"+serverip+":1337");
socket.on(JoinMessage.type, function (player) {
    console.log("Join Message received ")
    console.dir(player);
    match.players.push(player);
    if(host){
        if(match.players.length == 2){
            startGame();
        }
    } else if(match.players.length == 2){
        init();
    }
});

var btnCreate = document.getElementById("create");
btnCreate.addEventListener("click", function hostAGame(){
        document.getElementById("GameScreen").style.display = 'block';
        document.getElementById("GameScreen").style.background = 'linear-gradient(#000000, #222222) no-repeat';
        document.getElementById("GameScreen").style.textAlign = 'center';
        document.getElementById("GameScreen").style.textAlign = 'center';
        document.getElementById("container_2").style.display = 'none';
        currentPlayer.name =  "host"
           host = true;
        createMatch();
})

var btnJoin = document.getElementById("join");
btnJoin.addEventListener("click", function hostAGame(){
    currentPlayer.name = "invit";
    document.getElementById('JoinScreen').style.display = 'block';
    document.getElementById('container_2').style.display = 'none';
    getMatches();
})


const createMatch = function () {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://'+serverip+':3000/matches', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function (event) {
        if (this.readyState === XMLHttpRequest.DONE) {
            match = JSON.parse(this.response);
            initGameView();
            socket.emit(JoinRequest.type, new JoinRequest(match.id, currentPlayer));
        }
    };
    xhr.send(JSON.stringify({
        gameplayType: gameplayType,
        balls: [new Ball(getUID(), 250, 100, 0, "", {})]
    }));
}

const getMatches = function () {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://'+serverip+':3000/matches/'+gameplayType, true);
    xhr.onreadystatechange = function (event) {
        if (this.readyState === XMLHttpRequest.DONE) {
            let response = JSON.parse(this.response);
            console.log(response);
            displayMatches(response);
        }
    };
    xhr.send();
}


var displayMatches = function(matches){
    var joinDiv = document.getElementById('JoinScreen');

    for(var i=0; i < matches.length; i++){
        var element = document.createElement('button');
        element.innerHTML = matches[i].id;
        element.match = matches[i];
        element.addEventListener("click",function(){
            match = this.match;
            document.getElementById('GameScreen').style.display = 'block';
            document.getElementById('JoinScreen').style.display = 'none';
            initGameView();
            socket.emit(JoinRequest.type, new JoinRequest(match.id, currentPlayer));

        });
        joinDiv.appendChild(element);
    }
}

