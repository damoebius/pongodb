var stage = new createjs.Stage("demoCanvas");

var background = new createjs.Bitmap("images/background.png");
background.scale = 0.435;

var paddle = new createjs.Container();
var paddleBitmap = new createjs.Bitmap("images/raquette.png");
paddle.addChild(paddleBitmap);
paddleBitmap.x = -31;
paddleBitmap.y = -115;
paddleBitmap.scale = 0.6;

paddle.x = 320;
paddle.y = 350;

var paddleInvit = new createjs.Container();
var paddleBitmapInvit = new createjs.Bitmap("images/raquetteInvit.png");
paddleInvit.addChild(paddleBitmapInvit);
paddleBitmapInvit.x = -31;
paddleBitmapInvit.y = -115;
paddleBitmapInvit.scale = 0.6;

paddleInvit.x = 1100;
paddleInvit.y = 350;

var ball = new createjs.Container();
var ballBitmap = new createjs.Bitmap("images/ball.png");
ball.addChild(ballBitmap);
ballBitmap.x = -24;
ballBitmap.y = -31;
ball.scale = 0.8;

ball.x = 700 - 24;
ball.y = 400 - 31;

stage.addChild(background);
stage.addChild(paddleInvit);
stage.addChild(paddle);
stage.addChild(ball);

var ballSpeed = 8.0;


