var canvas = document.getElementById("demoCanvas");
var stage = new Stage("demoCanvas");
var player = new Shape();
var player2 = new Shape();
var player2Score = new Text('0', 'bold 35px Arial', '#ff2f56');
var playerScore = new Text('0', 'bold 35px Arial', '#6600ff');
var tkr = new Object;
var clock = document.getElementById("clockdiv");
var ball = new Shape();
var ballxSpeed = 8;
var ballySpeed = 8;


var initGameView = function() {
  player.graphics.beginFill("#6600ff").drawRect(-10, -40, 20, 150);
  player2.graphics.beginFill("#ff2f56").drawRect(-10, -40, 20, 150);
  ball.graphics.beginFill("#ff2f56").drawCircle(-3, -10, 10);
  playerScore.x = 300;
  playerScore.y = 60;
  player2Score.x = window.innerWidth - 300;
  player2Score.y = 60;
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight - 5;
  ball.x = 800;
  ball.y = 300;
  stage.addChild(ball, player, player2, playerScore, player2Score);
  player.x = 100;
  player2.x = window.innerWidth - 100;

  Ticker.setFPS(60);
  Ticker.addListener(stage);
}



var handleTick = function(event) {
  if (started) {
    update();
  }
  stage.update();
}


function startGame(e) {
  init();
  started = true;

}

var init = function() {
    socket.on(MoveBallMessage.type, function(message) {
      ball.x = message.x;
      ball.y = message.y;
    });

    // function reset() {
    //     ball.x = window.innerWidth/2;
    //     ball.y = window.innerHeight/2 + 30;
    //     player.y = window.innerHeight/2;
    //     player2.y = window.innerHeight/2;
    //
    //     stage.onMouseMove = null;
    //     Ticker.removeListener(tkr);
    //     stage.onPress = startGame;
    // }

    if ((ball.x) < 0) {
      ballxSpeed = -ballxSpeed;
      player2Score.text = parseInt(player2Score.text + 1);
      reset();
    }


    if ((ball.x) > window.innerWidth); {
      ballxSpeed = -ballxSpeed;
      playerScore.text = parseInt(playerScore.text + 1);
      reset();
    }

    var update = function() {
      socket.on(MoveBallMessage.type, function(message) {
        ball.x = ball.x + ballxSpeed;
        ball.y = ball.y + ballySpeed;
      });

      if ((ball.y) < 0) {
        ballySpeed = -ballySpeed
      };
      if ((ball.y + (30)) > 720) {
        ballySpeed = -ballySpeed
      };

      if ((ball.x) > window.innerWidth) {
        ballxSpeed = -ballxSpeed;
        playerScore.text = parseInt(playerScore.text + 1);
        // reset();
      }

      if ((ball.x) < 0) {
        ballxSpeed = -ballxSpeed;
        player2Score.text = parseInt(player2Score.text + 1);
        // reset();
      }

      if (ball.x + 10 > player.x && ball.x + 10 < player.x + 40 && ball.y >= player.y && ball.y < player.y + 150) {
        ballxSpeed *= -1;
      }
      if (ball.x - 10 < player2.x && ball.x - 10 > player2.x - 40 && ball.y >= player2.y && ball.y < player2.y + 150) {
        ballxSpeed *= -1;
      }

      if (player.y >= 800) {
        player.y = 800;
      }

      if (player2.y >= 800) {
        player2.y = 800;
      }

      if (playerScore.text == '10') {
        window.location = '../DodgePongNeon/win.html';
      }

      if (player2Score.text == '10') {
        window.location = '../DodgePongNeon/loose.html';
      }
      socket.emit(MoveBallRequest.type, new MoveBallRequest(match.balls[0], ballxSpeed, ballySpeed, 0))
    }



    function moveplayer(e) {
      player.y = e.stageY;
    }
