const socket = io("ws://localhost:1337");

var match;
var currentPlayer;

const getMatches = function () {
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:3000/matches/team', true);
    xhr.onreadystatechange = function (event) {
        if (this.readyState === XMLHttpRequest.DONE) {
            let response = JSON.parse(this.response);
            for(item of response){
                console.log(item.id)
            }
        }
    };
    xhr.send();
}

const createMatch = function () {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://localhost:3000/matches', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function (event) {
        if (this.readyState === XMLHttpRequest.DONE) {
            match = JSON.parse(this.response);
            console.dir(match);
            currentPlayer = new Player(Date.now(), 0, 0, 0, 0, "player1")
            socket.emit(JoinRequest.type, new JoinRequest(match.id, currentPlayer));
        }
    };
    xhr.send(JSON.stringify({
        gameplayType: 'team8',
        balls: [new Ball("first", 10, 10, 10, "", {})]
    }));
}




let btnCreateMatch = document.getElementById("createMatch");

btnCreateMatch.addEventListener("click", () => {
    createMatch(); 
    getMatches();    
})