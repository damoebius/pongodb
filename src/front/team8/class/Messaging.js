class Messaging {
    constructor(){
        this.socket = io("ws://localhost:1337");
        this.socket.on("connect", () => {
            this.init();
            console.log("Init done");
        });
        console.log("Messaging instance has been created");

    }

    init(){

        this.socket.on("ded", () => {
            let event = new Event("test");
            document.dispatchEvent(event);            
        });

    }

    movePalette(palette){
        socket.emit("moveReq", palette);
    }

}
