var stage = new createjs.Stage("demoCanvas");
   	var clavier={gauche:0, droite:0};
   	var clavier1= {gauche:0, droite:0};

    var bg = new createjs.Bitmap("images/bg.png");
    bg.scaleX = 0.5;
    bg.scaleY = 0.5;


       var player1 =
       // new createjs.Shape();
       // player1.graphics.beginFill("#ff0000").drawRect(10,40,100,20);
        new createjs.Container();
       player1.width = 100;
       var pl1bitmap = new createjs.Bitmap("images/goku.png");
       player1.addChild(pl1bitmap);
       pl1bitmap.scale = 0.05;

       player1.y = 30;
       console.log(player1);


       var player2 =
       // new createjs.Shape();
       // player2.graphics.beginFill("#00FF00").drawRect(10,40,100,20);
       new createjs.Bitmap("images/jiren.png");
       player2.scaleX=0.1;
       player2.scaleY=0.1;
       player2.x = 300;
       player2.y= 400;



       var ball = new createjs.Bitmap("images/ki.png");
       ball.scaleX=0.15;
	     ball.scaleY=0.15;

       ball.x = 480;
       ball.y = 270;

       stage.addChild(bg);
       stage.addChild(ball);
       stage.addChild(player1);
       stage.addChild(player2);

       var ballSpeed = 4.0;


       var ballXVector = 0.3;
       var ballYVector = 1;




    window.onkeydown=keyPress;
		//quand l'utilisateur relâche une touche, la fonction keyRelease est appelée
		window.onkeyup=keyRelease;
		//le paramètre "e" correspond à un événement
		function keyPress(e){
		//la touche "flèche gauche" porte le numéro 37
			if (e.keyCode == 37){
			//quand l'utilisateur enfonce la touche "flèche gauche", l'attribut "gauche" a pour valeur 1
			clavier.gauche = 1;
			}
			//la touche "flèche droite" porte le numéro 39
			if (e.keyCode == 39){
			//quand l'utilisateur enfonce la touche "flèche droite", l'attribut "droite" a pour valeur 1
			clavier.droite = 1;
			}

			if (e.keyCode == 81){
			//quand l'utilisateur enfonce la touche "flèche gauche", l'attribut "gauche" a pour valeur 1
			clavier1.gauche = 1;
			}
			//la touche "flèche droite" porte le numéro 39
			if (e.keyCode == 68){
			//quand l'utilisateur enfonce la touche "flèche droite", l'attribut "droite" a pour valeur 1
			clavier1.droite = 1;
			}
		}

		function keyRelease(e){
			if (e.keyCode == 37){
			//quand l'utilisateur relâche la touche "flèche gauche", l'attribut "gauche" a pour valeur 0
			clavier.gauche = 0;
			}
			if (e.keyCode == 39){
			//quand l'utilisateur relâche la touche "flèche droite", l'attribut "droite" a pour valeur 0
			clavier.droite = 0;
			}

			if (e.keyCode == 81){
			//quand l'utilisateur relâche la touche "flèche gauche", l'attribut "gauche" a pour valeur 0
			clavier1.gauche = 0;
			}
			if (e.keyCode == 68){
			//quand l'utilisateur relâche la touche "flèche droite", l'attribut "droite" a pour valeur 0
			clavier1.droite = 0;
			}
		}

		function deplacement(){
			if (clavier.gauche==1){
			//si clavier.gauche=1 alors on décrémente la position x du smiley
			player1.x=player1.x-5;
			}
			if (clavier.droite==1){
			//si clavier.droite=1 alors on incrémente la position x du smiley
			player1.x=player1.x+5	;
			}

			if (clavier1.gauche==1){
			//si clavier.gauche=1 alors on décrémente la position x du smiley
			player2.x=player2.x-5;
			}
			if (clavier1.droite==1){
			//si clavier.droite=1 alors on incrémente la position x du smiley
			player2.x=player2.x+5	;
			}
		}


		function collision(){
			if (ball.y <= player1.y + 50 && ball.y > player1.y && ball.x >= player1.x && ball.x < player1.x + 30)
      {
				ballSpeed *= -1.05;
			}
      if (ball.y <= player2.y + 40 && ball.y > player2.y && ball.x >= player2.x && ball.x < player2.x + 115.2)
       {
				ballSpeed *= -1.05;
			}
		}




       function moveBall(){

           var nextYPosition = ball.y + (ballSpeed*ballYVector);
           var nextXPosition = ball.x + (ballSpeed*ballXVector);

           if(nextYPosition > stage.canvas.height
           || nextYPosition <= 0
           ){
               ballYVector *= -1;

           }

           ball.y =  nextYPosition;

           if(nextXPosition > stage.canvas.width
           || nextXPosition <= 0)
           {
               ballXVector *= -1 ;

           }

           ball.x =  nextXPosition;

       }

       function handleTick(event) {
       		deplacement();
           moveBall();
           collision();
           stage.update();
       }

       createjs.Ticker.framerate  = 60;
       createjs.Ticker.addEventListener("tick", handleTick);
