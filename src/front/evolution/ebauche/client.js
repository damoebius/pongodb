
console.log("client script");
var serverip = "localhost";
var gameplayType="test";
var myBall="test";
var match;
var currentPlayer = new Player(getUID(), 0, 0, 0, 0, "damoebius");
const socket = io("ws://"+serverip+":1337");

socket.on(JoinMessage.type, function (message) {
    console.log("Join Message received ")
    console.dir(message);
    createjs.Ticker.framerate = 60;
    createjs.Ticker.addEventListener("tick", handleTick);
    console.dir(player);
    match.players.push(player);
});

var getUID = function(){
    return new Date.now();
}
var hostAGame = function() {
    document.getElementById("HostScreen").style.display = "block";
    document.getElementById("HomeScreen").style.display = "none";
    currentPlayer.name = "player1";
    createMatch();
}
var joinAGame = function() {
    document.getElementById("JoinScreen").style.display = "block";
    document.getElementById("HomeScreen").style.display = "none";
    currentPlayer.name="player2";
}


const createMatch = function () {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://"+serverip+":3000/matches', true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function (event) {
        if (this.readyState === XMLHttpRequest.DONE) {
            match = JSON.parse(this.response);
            console.dir(match);
            socket.emit(JoinRequest.type, new JoinRequest(match.id, currentPlayer));
        }
    };
    xhr.send(JSON.stringify({
        gameplayType: gameplayType,
        balls: [new Ball(myBall, 10, 10, 10, "", {})]
    }));
}

fucntion getUID(){
    return new Date().getTime();
}