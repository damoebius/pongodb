var stage = new createjs.Stage("demoCanvas");
var paddle1 = new createjs.Shape();
var paddle2 = new createjs.Shape();
var ball = new createjs.Shape();
var started = false;
var ballSpeed = 4.0;
var ballXVector = 1;
var ballYVector = 1;


var initGameView = function () {
    paddle1.graphics.beginFill("#ff0000").drawRect(-10, -25, 20, 50);
    paddle2.graphics.beginFill("#00ff00").drawRect(-10, -25, 20, 50);
    ball.graphics.beginFill("#0000ff").drawCircle(-5, -5, 10);

    ball.x = 250; ball.y = 100;
    paddle1.x = 75; paddle1.y = 100;
    paddle2.x = 425; paddle2.y = 100;


    stage.addChild(ball);
    stage.addChild(paddle1);
    stage.addChild(paddle2);

    createjs.Ticker.framerate = 60;
    createjs.Ticker.addEventListener("tick", handleTick);
}

var handleTick = function (event) {
    if(started){
        moveBall();
    }
    stage.update();
}

var startGame = function(){
    init();
    started = true;
}

var init = function(){
    socket.on(MoveBallMessage.type, function (message) {
        ball.x = message.x;
        ball.y = message.y
    });
}

function moveBall() {

    var nextYPosition = ball.y + (ballSpeed * ballYVector);
    var nextXPosition = ball.x + (ballSpeed * ballXVector);

    if (nextYPosition > stage.canvas.height
        || nextYPosition <= 0
    ) {
        ballYVector *= -1;
    }

    if (nextXPosition > stage.canvas.width
        || nextXPosition <= 0
    ) {
        ballXVector *= -1;
    }

    socket.emit(MoveBallRequest.type, new MoveBallRequest(match.balls[0],nextXPosition,nextYPosition,0))

}