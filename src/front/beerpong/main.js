var elem = document.querySelector( 'div#balle' ),
    centrex = 0,
    centrey = 0,
    mxOffset = 0,
    myOffset = 0,
    positionx = 500,
    positiony = 0,
    ew = elem.offsetWidth,
    eh = elem.offsetHeight,
    vx = 0,
    vy = 0,
    ww = window.innerWidth,
    wh = window.innerHeight,
    tracking = false,
    raf = null,
    prefixes = [ '-o-', '-ms-', '-moz-', '-webkit-', '' ];

    var table = document.querySelector( 'div#table' ),
        tablepositiony = 0,
        tablepositionx = 0,
        tw = table.offsetWidth,
        th = table.offsetHeight,
        prefixes = [ '-o-', '-ms-', '-moz-', '-webkit-', '' ];

function prefixCss( elem, prop, val ) {
	  var length = prefixes.length,
		  i = 0;
	  for( ; i < length; i++ ) {
		    elem.style[ prefixes[ i ] + prop ] = val;
	  }
}

function prefixCsstable( table, prop, val ) {
	  var length = prefixes.length,
		  i = 0;
	  for( ; i < length; i++ ) {
        table.style[ prefixes[ i ] + prop ] = val;
	  }
}

function setElemCoords( x, y ) {
  prefixCss( elem, 'transform', 'translate3d( ' + x + 'px, ' + y + 'px, 0)'  );
  elem.setAttribute( 'data-x', x );
  elem.setAttribute( 'data-y', y );
  positionx = x;
  positiony = y;

}

function setTableCoords( x, y ) {
  prefixCsstable( table, 'transform', 'translate3d( ' + x + 'px, ' + y + 'px, 0)'  );
  table.setAttribute( 'data-x', x );
  table.setAttribute( 'data-y', y );
  tablepositionx = x;
  tablepositiony = y;

}

function checkBounds() {
  if( positionx + ew > ww ) {
    if( tracking ) {
      vx = 0;
    } else {
      vx = -vx * 0.7;
      vy *= 0.99;
    }
    positionx = ww - ew;
  }

  if( positionx < 0 ) {
    if( tracking ) {
      vx = 0;
    } else {
      vx = -vx * 0.7;
      vy *= 0.99;
    }
    positionx = 0;
  }

  if( positiony + eh > wh ) {
    if( tracking ) {
      vy = 0;
    } else {
      vx *= 0.99;
      vy = -vy * 0.7;
    }
    positiony = wh - eh;
  }

  if( positiony < 0 ) {
    if( tracking ) {
      vy = 0;
    } else {
      vx *= 0.99;
      vy = -vy * 0.7;
    }
    positiony = 0;
  }
}

function hittest() {
  if(positiony <= tablepositiony ) {

    }
}



function mousedowncb() {
  tracking = true;
  setElemCoords( positionx, positiony );
  mxOffset = centrex - positionx;
  myOffset = centrey - positiony;
}

function mouseupcb() {
  tracking = false;
}

function mousemovecb( e ) {
  centrex = e.clientX;
  centrey = e.clientY;
}

function resizecb() {
  ww = window.innerWidth;
  wh = window.innerHeight;
}

function loop() {
  raf = requestAnimationFrame( loop );
  if( tracking ) {
    vx = ( centrex - mxOffset - positionx ) / 4;
    vy = ( centrey - myOffset - positiony ) / 4;
  }
  vy += 0.9;
  vx *= 0.99;
  vy *= 0.99;
  positionx += vx;
  positiony += vy;

  checkBounds();





  setElemCoords( positionx, positiony );
  setTableCoords( tablepositionx, tablepositiony );
}



// bind events
elem.addEventListener( 'mousedown', mousedowncb, false );
window.addEventListener( 'mouseup', mouseupcb, false );
window.addEventListener( 'mousemove', mousemovecb, false );
window.addEventListener( 'resize', resizecb, false );

loop();
